#include "parser.h"

#include <iostream>
#include <libxml2/libxml/xmlreader.h>


static void processNode(xmlTextReaderPtr reader) {
    const xmlChar *name, *value;

    name = xmlTextReaderConstName(reader);
    if (name == NULL)
        name = BAD_CAST "--";

    value = xmlTextReaderConstValue(reader);


    printf("%d %d %s %d %d %d %s",
        xmlTextReaderDepth(reader),
        xmlTextReaderNodeType(reader),
        name,
        xmlTextReaderIsEmptyElement(reader),
        xmlTextReaderHasValue(reader),
           xmlTextReaderAttributeCount(reader),
           xmlTextReaderReadAttributeValue(reader));


    while ( xmlTextReaderMoveToNextAttribute (reader))
        printf (" - %s: %s\n", xmlTextReaderConstName (reader), xmlTextReaderConstValue (reader));





    if (value == NULL)
    printf("\n");
    else {
        if (xmlStrlen(value) > 40)
            printf(" %.40s...\n", value);
        else
        printf(" %s\n", value);
    }
}



parser::parser()
{
    xmlTextReaderPtr reader;
    reader = xmlReaderForFile("/home/grek/test.xml",
                              NULL,
                              XML_PARSE_NOENT );
    if(reader == NULL)
    {
        std::cerr << "Failed to load xml file." << std::endl;
        return;
    }

    int ret = xmlTextReaderRead(reader);
    while(ret == 1)
    {
        processNode(reader);
        ret = xmlTextReaderRead(reader);
    }
}
