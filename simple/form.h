#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QKeyEvent>
#include <QTextEdit>
#include <QFocusEvent>
#include <QPaintEvent>
#include <QMap>
#include <QTimer>

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    Q_PROPERTY(QString help READ helpText)

    explicit Form(QWidget *parent = 0);
    ~Form();

protected:
    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent * event);
    void focusInEvent(QFocusEvent * event);
    void focusOutEvent(QFocusEvent * event);
    void paintEvent(QPaintEvent * event);

    QString helpText();

private slots:
    void logic();

private:
    QMap<int, bool>keys;
    int code[10];
    int othercode[10] = {Qt::UpArrow, Qt::UpArrow, Qt::DownArrow, Qt::DownArrow, Qt::LeftArrow, Qt::RightArrow, Qt::LeftArrow, Qt::RightArrow, Qt::Key_B, Qt::Key_A};
    Ui::Form *ui;
    QTimer timing;

};

#endif // FORM_H
