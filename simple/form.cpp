#include "form.h"
#include "ui_form.h"
#include <qdebug.h>
#include <QStyleOption>
#include <QPainter>
#include <QMapIterator>

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
    setFocusPolicy(Qt::StrongFocus);
    connect(&timing, SIGNAL(timeout()),SLOT(logic()));
    timing.setInterval(1000/10);
    timing.start();

    qDebug() << "creater";
}

Form::~Form()
{
    delete ui;
}

void Form::keyPressEvent(QKeyEvent *event)
{
    keys[event->key()] = true;

}

void Form::keyReleaseEvent(QKeyEvent *event)
{
    keys[event->key()] = false;
}

void Form::focusInEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    qDebug() << "I see you!";

    setStyleSheet("QWidget{border: 1px solid gray;border-radius: 3px;padding: 1px 18px 1px 3px;min-width: 6em;}");
}

void Form::focusOutEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    qDebug() << "No don't run!!!";
    setStyleSheet("");

    QMap<int, bool>::iterator it;
    for (it=keys.begin(); it != keys.end();it++){
        keys[it.key()] = false;
    }
}

#include <algorithm> // for std::find
#include <iterator> // for std::begin, std::end

void Form::paintEvent(QPaintEvent *event)
{
    qDebug() << "asdf";
    Q_UNUSED(event);
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);

    QMap<int, bool>::iterator it;
    for (it=keys.begin(); it != keys.end();it++){
        keys[it.key()] = false;
    }
}

QString Form::helpText()
{
    return QString("heeelp!");
}

void Form::logic()
{
    if(keys[Qt::Key_Escape]){
        qDebug() << "escape!!!";
    }
    if(keys[Qt::Key_1]){
        qDebug() << "now!!!";
    }
    if(code == othercode){
        qDebug() << "yeeey";
    }
}


